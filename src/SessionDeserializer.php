<?php

namespace Drupal\session_inspector;

/**
 * A utility class to deserialize PHP session data.
 *
 * Adapted from https://www.php.net/manual/en/function.session-decode.php#108037
 *
 * @package Drupal\session_inspector
 */
class SessionDeserializer {

  /**
   * Method to deserialize a string of PHP session data.
   *
   * This method will detect what type of session data is stored and hand off
   * the extraction to other methods.
   *
   * @param string $sessionData
   *   The session data.
   *
   * @return array
   *   The deserialized array of session data.
   *
   * @throws \Exception
   */
  public static function deserialize($sessionData):array {
    $method = ini_get("session.serialize_handler");

    switch ($method) {
      case "php":
        return self::deserializePhp($sessionData);

      case "php_binary":
        return self::deserializePhpBinary($sessionData);

      default:
        throw new \Exception("Unsupported session.serialize_handler: " . $method . ". Supported: php, php_binary");
    }
  }

  /**
   * Extract the session data from a PHP session string.
   *
   * @param string $sessionData
   *   The session data.
   *
   * @return array
   *   The deserialized array of session data.
   */
  private static function deserializePhp($sessionData):array {
    // Store the current session data in a variable.
    $session = $_SESSION;

    // Destroy the current session.
    session_destroy();

    // Create a new session and populate it with the passed session data.
    session_start();
    session_decode($sessionData);

    // Extract the decoded session data.
    $returnData = $_SESSION;

    // Destroy our temporary session.
    session_destroy();

    // Create a new session for the user and populate it with their current
    // session data.
    session_start();
    $_SESSION = $session;

    // Return the extracted session data.
    return $returnData;
  }

  /**
   * Extract the session data from a PHP Binary session string.
   *
   * @param string $sessionData
   *   The session data.
   *
   * @return array
   *   The deserialized array of session data.
   *
   * @throws \Exception
   */
  private static function deserializePhpBinary($sessionData):array {
    $returnData = [];
    $offset = 0;

    while ($offset < strlen($sessionData)) {
      $num = ord($sessionData[$offset]);
      $offset += 1;
      $varname = substr($sessionData, $offset, $num);
      $offset += $num;
      $data = unserialize(substr($sessionData, $offset), ['allowed_classes' => FALSE]);
      $returnData[$varname] = $data;
      $offset += strlen(serialize($data));
    }

    return $returnData;
  }

}
