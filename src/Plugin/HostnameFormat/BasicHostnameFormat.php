<?php

namespace Drupal\session_inspector\Plugin\HostnameFormat;

use Drupal\Core\Plugin\PluginBase;
use Drupal\session_inspector\Plugin\HostnameFormatInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides basic formatting for hostname.
 *
 * @HostnameFormat(
 *   id = "basic",
 *   name = @Translation("Basic hostname format")
 * )
 */
class BasicHostnameFormat extends PluginBase implements HostnameFormatInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formatHostname(string $hostname):string {
    return $hostname;
  }

}
