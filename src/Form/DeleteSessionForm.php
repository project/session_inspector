<?php

namespace Drupal\session_inspector\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\session_inspector\SessionDeletionInterface;
use Drupal\session_inspector\SessionInspectorInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to allow users to delete sessions.
 *
 * @package Drupal\session_inspector\Form
 */
class DeleteSessionForm extends ConfirmFormBase {

  /**
   * The user object.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * The session ID.
   *
   * @var string
   */
  protected string $sessionId;

  /**
   * The SessionInspector service.
   *
   * @var \Drupal\session_inspector\SessionInspectorInterface
   */
  protected SessionInspectorInterface $sessionInspector;

  /**
   * The session inspector delete all sessions service.
   *
   * @var \Drupal\session_inspector\SessionDeletionInterface
   */
  protected SessionDeletionInterface $deleteAllSessionsService;

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL, $sid = NULL) {
    $this->user = $user;
    $this->sessionId = $sid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self();
    $instance->sessionInspector = $container->get('session_inspector');
    $instance->deleteAllSessionsService = $container->get('session_inspector.session_deletion');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t('Delete the session?');
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return new Url('session_inspector.manage', ['user' => $this->user->id()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'session_inspector_delete_form';
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the session.
    $this->deleteAllSessionsService->deleteSession($this->sessionId);

    // Redirect the user back to the session list.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
